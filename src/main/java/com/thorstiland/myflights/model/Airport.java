package com.thorstiland.myflights.model;

import javax.persistence.Embeddable;

@Embeddable
public class Airport {
	private String code;
	private String name;
	private String openflightsId;

	private double latitude;
	private double longitude;
	private String timezoneId;

	public String getTimezoneId() {
		return timezoneId;
	}

	public void setTimezoneId(String timezoneId) {
		this.timezoneId = timezoneId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOpenflightsId() {
		return openflightsId;
	}

	public void setOpenflightsId(String openflightsId) {
		this.openflightsId = openflightsId;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "ModelAirport [code=" + code + ", name=" + name + ", openflightsId=" + openflightsId + ", latitude="
				+ latitude + ", longitude=" + longitude + "]";
	}

}
