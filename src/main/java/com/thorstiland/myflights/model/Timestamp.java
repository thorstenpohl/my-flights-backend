package com.thorstiland.myflights.model;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

@Embeddable
public class Timestamp {

	@Temporal(TemporalType.TIMESTAMP)
	private Date date;

	private String timezoneId;

	public String getTimezoneId() {
		return timezoneId;
	}

	public void setTimezoneId(String timezoneId) {
		this.timezoneId = timezoneId;
		update();
	}

	@Transient
	private String local;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
		update();
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
		update();
	}

	private void update(){
		if (this.local==null){
			if (this.date!=null && this.timezoneId !=null){
				this.local = this.localDate().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME);
			}
		} else if (this.date == null){
			if (this.local!=null && this.timezoneId !=null){
				this.date = Date.from(LocalDateTime.parse(this.local, DateTimeFormatter.ISO_LOCAL_DATE_TIME)
						.toInstant(ZoneOffset.of(timezoneId)));
			}
		}
	}
	public LocalDateTime localDate() {
		return LocalDateTime.ofInstant(date.toInstant(), ZoneId.of(this.timezoneId));
	}

	public ZonedDateTime zonedDate() {
		return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.of(this.timezoneId));
	}

	@Override
	public String toString() {
		return "Timestamp [date=" + date + ", timezoneId=" + timezoneId + ", local=" + local + ", getLocalDate()="
				+ localDate() + ", getZonedDate()=" + zonedDate() + "]";
	}
	
	
}
