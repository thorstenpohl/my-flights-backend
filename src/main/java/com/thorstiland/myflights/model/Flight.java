package com.thorstiland.myflights.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.GenericGenerator;

@Entity
public class Flight {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	private String userId;
	
	private Timestamp departure;

	private Timestamp arrival;
	
	private Airport from;
	private Airport to;
	private String flightNo;
	private String seat;
	private String seatType;
	private String bookingClass;
	private String reason;
	private Airline carrier;
	private Aircraft aircraft;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public Airport getFrom() {
		return from;
	}
	public void setFrom(Airport from) {
		this.from = from;
	}
	public Airport getTo() {
		return to;
	}
	public void setTo(Airport to) {
		this.to = to;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public String getSeat() {
		return seat;
	}
	public void setSeat(String seat) {
		this.seat = seat;
	}
	public String getSeatType() {
		return seatType;
	}
	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}
	public String getBookingClass() {
		return bookingClass;
	}
	public void setBookingClass(String bookingClass) {
		this.bookingClass = bookingClass;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Airline getCarrier() {
		return carrier;
	}
	public void setCarrier(Airline carrier) {
		this.carrier = carrier;
	}
	public Aircraft getAircraft() {
		return aircraft;
	}
	public void setAircraft(Aircraft aircraft) {
		this.aircraft = aircraft;
	}
	
	
	
	
	public Timestamp getDeparture() {
		return departure;
	}
	public void setDeparture(Timestamp departure) {
		this.departure = departure;
	}
	public Timestamp getArrival() {
		return arrival;
	}
	public void setArrival(Timestamp arrival) {
		this.arrival = arrival;
	}
	@Override
	public String toString() {
		return "Flight [id=" + id + ", userId=" + userId + ", departure=" + departure + ", arrival=" + arrival
				+ ", from=" + from + ", to=" + to + ", flightNo=" + flightNo + ", seat=" + seat + ", seatType="
				+ seatType + ", bookingClass=" + bookingClass + ", reason=" + reason + ", carrier=" + carrier
				+ ", aircraft=" + aircraft + "]";
	}

	
	
}
