@XmlJavaTypeAdapters({
    @XmlJavaTypeAdapter(type = java.time.LocalDateTime.class, 
                        value = LocalDateAdapter.class),
    @XmlJavaTypeAdapter(type = java.time.ZonedDateTime.class, 
    					value = ZonedDateAdapter.class)
})
package com.thorstiland.myflights.model;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapters;

import com.thorstiland.myflights.rest.util.LocalDateAdapter;
import com.thorstiland.myflights.rest.util.ZonedDateAdapter;