package com.thorstiland.myflights.model;

import javax.persistence.Embeddable;

@Embeddable
public class Airline {
	private String airlineCode;
	private String airlineName;

	public String getAirlineCode() {
		return airlineCode;
	}

	public void setAirlineCode(String airlineCode) {
		this.airlineCode = airlineCode;
	}

	public String getAirlineName() {
		return airlineName;
	}

	public void setAirlineName(String airlineName) {
		this.airlineName = airlineName;
	}

	@Override
	public String toString() {
		return "ModelAirline [airlineCode=" + airlineCode + ", airlineName=" + airlineName + "]";
	}

}
