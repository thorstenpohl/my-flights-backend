package com.thorstiland.myflights.model;

import javax.persistence.Embeddable;

@Embeddable
public class Aircraft {

	private String aircraftType;
	private String tailsign;
	private String aircraftCode;

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getTailsign() {
		return tailsign;
	}

	public void setTailsign(String tailsign) {
		this.tailsign = tailsign;
	}

	public String getAircraftCode() {
		return aircraftCode;
	}

	public void setAircraftCode(String aircraftCode) {
		this.aircraftCode = aircraftCode;
	}

	@Override
	public String toString() {
		return "ModelAircraft [aircraftType=" + aircraftType + ", tailsign=" + tailsign + ", aircraftCode="
				+ aircraftCode + "]";
	}

}
