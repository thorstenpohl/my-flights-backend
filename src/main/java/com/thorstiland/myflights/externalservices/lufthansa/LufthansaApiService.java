package com.thorstiland.myflights.externalservices.lufthansa;

import static com.thorstiland.myflights.externalservices.JSONUtils.extractDate;
import static com.thorstiland.myflights.externalservices.JSONUtils.getObject;
import static com.thorstiland.myflights.externalservices.JSONUtils.getString;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.inject.Inject;
import javax.inject.Named;
import javax.json.JsonObject;
import javax.ws.rs.ServerErrorException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thorstiland.myflights.externalservices.geonames.GeonamesApiService;
import com.thorstiland.myflights.externalservices.lufthansa.model.FlightStatus;
import com.thorstiland.myflights.externalservices.lufthansa.model.LhApiAircraft;
import com.thorstiland.myflights.externalservices.lufthansa.model.LhApiAirport;

@Named
public class LufthansaApiService {
	public static final String LH_API_DATEFORMAT = "yyyy-MM-dd'T'HH:mm";
	private static final Logger LOG = LoggerFactory.getLogger(LufthansaApiService.class);
	@Inject
	LufthansaAPI lhApi;
	
	@Inject GeonamesApiService geonamesApiService;

	/**
	 * Gets the Flight Status from the Lufthansa API.
	 * 
	 * @param flightNo
	 *            the flight-number you are interested in.
	 * @param date
	 *            the Date formatted "yyyy-MM-dd". not null.
	 * @return a FlightStatus which might be empty, if nothing was found found.
	 */
	public FlightStatus getFlightStatus(final String flightNo, final String date) {
		final FlightStatus status = new FlightStatus();
		try {

			
			Client client = ClientBuilder.newClient();
			WebTarget target = client.target(
					UriBuilder.fromPath("https://api.lufthansa.com/v1/operations/flightstatus/{flightNo}/{date}")
							.build(flightNo, date));
			JsonObject jsonStatus = lhApi.authenticateBuilder(target.request(MediaType.APPLICATION_JSON))
					.get(JsonObject.class);
			LOG.info("Flight-Status JSON: {}", jsonStatus);

			if (jsonStatus != null) {
				try {
					final JsonObject flight = getObject(jsonStatus, "FlightStatusResource.Flights.Flight");

					status.setFrom(getString(flight, "Departure.AirportCode"));
					
					Date departureUTC = extractDate(flight, LH_API_DATEFORMAT, "Departure.ActualTimeUTC.DateTime",
							"Departure.ScheduledTimeUTC.DateTime");
//					Date departureLocal = extractDate(flight, LH_API_DATEFORMAT, "Departure.ActualTimeLocal.DateTime",
//							"Departure.ScheduledTimeLocal.DateTime");
					status.setDeparture(ZonedDateTime.ofInstant(departureUTC.toInstant(), ZoneId.of("Z")));
//					status.setDepartureLocal(LocalDateTime.ofInstant(departureLocal.toInstant(), ZoneId.systemDefault()));
					

					status.setTo(getString(flight, "Arrival.AirportCode"));
					Date arrivalUTC = extractDate(flight, LH_API_DATEFORMAT, "Arrival.ActualTimeUTC.DateTime",
							"Arrival.ScheduledTimeUTC.DateTime");
//					Date arrivalLocal = extractDate(flight, LH_API_DATEFORMAT, "Arrival.ActualTimeLocal.DateTime",
//							"Arrival.ScheduledTimeLocal.DateTime");
					status.setArrival(ZonedDateTime.ofInstant(arrivalUTC.toInstant(), ZoneId.of("Z")));
//					status.setArrivalLocal(LocalDateTime.ofInstant(arrivalLocal.toInstant(), ZoneId.systemDefault()));
					status.setAirlineCode(getString(flight, "OperatingCarrier.AirlineID"));
					status.setAircraftCode(getString(flight, "Equipment.AircraftCode"));
					status.setFlightNo(flightNo);
					LOG.debug("Status {}", status);
				} catch (NullPointerException npe) {
					LOG.warn("Cannot extract status from JSON {}", jsonStatus, npe);
				}
			} else {
				LOG.info("Not found: Flight No: {} @ {}", flightNo, date);
			}

		} catch (ServerErrorException e) {
			LOG.info("Server-Error in LH-API", e);
		} catch (Exception e) {
			LOG.warn("Unexpected-Error in LH-API", e);
		}
		return status;
	}

	/**
	 * Gets Airport Information.
	 * 
	 * @param airportCode
	 *            the Airport Code
	 * @return the Airport - might be empty, if not found.
	 */
	public LhApiAirport getAirportInfo(final String airportCode) {
		final LhApiAirport airport = new LhApiAirport();
		try {
			Client client = ClientBuilder.newClient();

			WebTarget target = client
					.target(UriBuilder.fromPath("https://api.lufthansa.com/v1/references/airports/{airportCode}")
							.queryParam("lang", "en").queryParam("LHoperated", "false").build(airportCode));

			JsonObject json = lhApi.authenticateBuilder(target.request(MediaType.APPLICATION_JSON))
					.get(JsonObject.class);
			LOG.info("Airport JSON: {}", json);

			if (json != null) {
				try {
					final JsonObject jsonAirport = getObject(json, "AirportResource.Airports.Airport");
					airport.setCode(getString(jsonAirport, "AirportCode"));
					airport.setName(getString(jsonAirport, "Names.Name.$"));
					airport.setLongitude(Double.parseDouble(getString(jsonAirport, "Position.Coordinate.Longitude")));
					airport.setLatitude(Double.parseDouble(getString(jsonAirport, "Position.Coordinate.Latitude")));
					
					// Load timezone
					airport.setTimezoneId(geonamesApiService.loadTimezone(airport.getLongitude(), airport.getLatitude()));
					LOG.debug("Airport {}", airport);
				} catch (NullPointerException npe) {
					LOG.warn("Cannot extract airport from JSON {}", airport, npe);
				}
			}

		} catch (ServerErrorException e) {
			LOG.info("Server-Error in LH-API", e);
		} catch (Exception e) {
			LOG.warn("Unexpected-Error in LH-API", e);
		}
		return airport;
	}

	public LhApiAircraft getAircraftInfo(final String aircraftCode) {
		final LhApiAircraft aircraft = new LhApiAircraft();
		try {
			Client client = ClientBuilder.newClient();

			WebTarget target = client
					.target(UriBuilder.fromPath("https://api.lufthansa.com/v1/references/aircraft/{aircraftCode}")
							.queryParam("lang", "en").build(aircraftCode));

			JsonObject json = lhApi.authenticateBuilder(target.request(MediaType.APPLICATION_JSON))
					.get(JsonObject.class);
			LOG.info("Aircraft JSON: {}", json);

			if (json != null) {
				try {
					final JsonObject jsonAircraft = getObject(json,
							"AircraftResource.AircraftSummaries.AircraftSummary");
					aircraft.setAircraftCode(getString(jsonAircraft, "AircraftCode"));
					aircraft.setAircraftType(getString(jsonAircraft, "Names.Name.$"));

					LOG.info("Found Aircraft {}", aircraft);
				} catch (NullPointerException npe) {
					LOG.warn("Cannot extract airport from JSON {}", aircraft, npe);
				}
			}

		} catch (ServerErrorException e) {
			LOG.info("Server-Error in LH-API", e);
		} catch (Exception e) {
			LOG.warn("Unexpected-Error in LH-API", e);
		}
		return aircraft;
	}
}
