package com.thorstiland.myflights.externalservices.geonames;

import static com.thorstiland.myflights.externalservices.JSONUtils.getString;

import javax.inject.Named;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
public class GeonamesApiService {
	private static final Logger LOG = LoggerFactory.getLogger(GeonamesApiService.class);

	public String loadTimezone(double longitude, double latitude) {
		try {
			Client client = ClientBuilder.newClient();

			WebTarget target = client.target(UriBuilder.fromPath("http://api.geonames.org/timezoneJSON")
					.queryParam("lat", latitude).queryParam("lng", longitude).queryParam("username", "thpohl").build());
			JsonObject json = target.request(MediaType.APPLICATION_JSON).get(JsonObject.class);
			return getString(json, "timezoneId");
		} catch (Exception e) {
			LOG.warn("Cannot load the timezone.", e);
			throw e;
		}
	}
}
