package com.thorstiland.myflights.rest.util;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class ZonedDateAdapter extends XmlAdapter<String, ZonedDateTime> {

    @Override
    public ZonedDateTime unmarshal(String dateString) throws Exception {
        return ZonedDateTime.parse(dateString, DateTimeFormatter.ISO_DATE_TIME);
    }

    @Override
    public String marshal(ZonedDateTime localDate) throws Exception {
        return DateTimeFormatter.ISO_DATE_TIME.format(localDate);
    }
}
