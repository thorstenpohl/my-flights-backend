package com.thorstiland.myflights.rest.util;

import java.time.LocalDateTime;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

/**
 * This fixes the issue that {@link LocalDateTime} is not working in
 * Rest-Services. I do not like that this assumes we are using jackson..
 * 
 * @author thorsten
 *
 */
@Provider
@Produces(MediaType.APPLICATION_JSON)
public class JacksonConfig implements ContextResolver<ObjectMapper>
{
    private final ObjectMapper objectMapper;

    public JacksonConfig() throws Exception
    {
    	 objectMapper = new ObjectMapper()
                 .disable( SerializationFeature.WRITE_DATES_AS_TIMESTAMPS )
                 .disable( SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS )
                 .setSerializationInclusion( JsonInclude.Include.NON_NULL )
                 .registerModule( new JavaTimeModule() );
    }

    @Override
    public ObjectMapper getContext(Class<?> arg0)
    {
        return objectMapper;
    }
 }