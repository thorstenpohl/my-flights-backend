package com.thorstiland.myflights.rest;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thorstiland.myflights.dto.ExtendedFlightDto;
import com.thorstiland.myflights.dto.FlightDto;
import com.thorstiland.myflights.externalservices.lufthansa.LufthansaApiService;
import com.thorstiland.myflights.externalservices.lufthansa.model.FlightStatus;
import com.thorstiland.myflights.model.Aircraft;
import com.thorstiland.myflights.model.Airline;
import com.thorstiland.myflights.model.Flight;
import com.thorstiland.myflights.model.Timestamp;

import io.swagger.annotations.Api;

@Api(tags = { "Flights" ,"Autocomplete"})
@Path("flight-autocomplete")
public class FlightAutocompleteEndpoint {
	private static final Logger LOG = LoggerFactory.getLogger(FlightAutocompleteEndpoint.class);
	@Inject
	LufthansaApiService lufthansaApiService;

	@GET
	@Path("flightNo/{flightNo}")
	public ExtendedFlightDto autocomplete(@PathParam("flightNo") String flightNo){
		return this.autocomplete(flightNo,  new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
	}

	@GET
	@Path("flightNo/{flightNo}/{date}")
	public ExtendedFlightDto autocomplete(@PathParam("flightNo") String flightNo, @PathParam("date") String date){
		FlightDto f = new FlightDto();
		f.setFlightNo(flightNo);
		//FIXME use the date;
		//f.setDeparture(new Date());
		return this.autocompleteByFlightNumber(f);
	}
	
	@POST
	@Path("flightNo")
	public ExtendedFlightDto autocompleteByFlightNumber(FlightDto basicFlight) {
		ExtendedFlightDto flight = ExtendedFlightDto.fromBasicDto(basicFlight);
		if (flight.getFlightNo() != null) {
			try {
				// TODO better. Date handling.

				// Find the date of the flight.
				final String dateOfFlight;
				if (flight.getDeparture() != null && flight.getDeparture()!=null) {
					dateOfFlight = new SimpleDateFormat("yyyy-MM-dd").format(flight.getDeparture());
					//flight.getDeparture().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
				} 
//				else if (flight.getDepartureLocal() != null) {
//					dateOfFlight = flight.getDepartureLocal().format(DateTimeFormatter.ofPattern("yyyy.MM.dd"));
//				} 
				else {
					dateOfFlight = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
				}

				final FlightStatus lhStatus = lufthansaApiService.getFlightStatus(flight.getFlightNo(), dateOfFlight);
				if (lhStatus.getFrom() != null) {
					// Only if we found something
					flight.setOrigin(lufthansaApiService.getAirportInfo(lhStatus.getFrom()));
					flight.setDestination(lufthansaApiService.getAirportInfo(lhStatus.getTo()));
					
					// FIXME reimplement this.
//					final Timestamp departure = new Timestamp();
//					departure.setTimezoneId(flight.getFrom().getTimezoneId());
//					departure.setDate(Date.from(lhStatus.getDeparture().toInstant()));
//					flight.setDeparture(departure);
//					
//					
//					final Timestamp arrival = new Timestamp();
//					arrival.setTimezoneId(flight.getTo().getTimezoneId());
//					arrival.setDate(Date.from(lhStatus.getArrival().toInstant()));
//					flight.setArrival(arrival);

					final AircraftDto acInfo = lufthansaApiService.getAircraftInfo(lhStatus.getAircraftCode());
					flight.setAircraft(acInfo);	
					
				    final Airline airline = new Airline();
				    airline.setAirlineCode(lhStatus.getAirlineCode());
				    flight.setCarrier(airline);
				   }
			} catch (Exception e) {
				LOG.info("Problem in LH-API", e);
			}
		}

//		if (flight.getFlightNo() != null) {
//			String airlineId = flight.getFlightNo().substring(0, 2);
//			Airline airline = openflightsApiService.loadAirline(airlineId);
//
//			flight.setCarrier(airline);
//		}
//
//		if (flight.getFrom() != null) {
//			flight.setAptFrom(openflightsApiService.loadAirport(flight.getFrom()));
//		}
//		if (flight.getTo() != null) {
//			flight.setAptTo(openflightsApiService.loadAirport(flight.getTo()));
//		}
//
//		TimeZoneUtils.updateTimezones(flight);
		return flight;
	}
}
