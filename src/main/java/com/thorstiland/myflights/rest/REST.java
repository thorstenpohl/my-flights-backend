package com.thorstiland.myflights.rest;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

import com.thorstiland.myflights.rest.util.JacksonConfig;

import io.swagger.jaxrs.config.BeanConfig;

@ApplicationPath("/rest")
public class REST extends Application {
	public REST() {

		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setVersion("0.0.1");
		beanConfig.setSchemes(new String[] { "https" });
		beanConfig.setBasePath("/rest");
		beanConfig.setResourcePackage("com.thorstiland.myflights.rest");
		beanConfig.setScan(true);

	}
	
}
