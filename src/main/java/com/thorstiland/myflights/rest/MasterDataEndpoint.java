package com.thorstiland.myflights.rest;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thorstiland.myflights.dto.AirportDto;
import com.thorstiland.myflights.externalservices.lufthansa.LufthansaApiService;

import io.swagger.annotations.Api;

@Api(tags = { "Masterdata", "Autocomplete" })
@Path("masterdata")
public class MasterDataEndpoint {
	private static final Logger LOG = LoggerFactory.getLogger(MasterDataEndpoint.class);
	@Inject
	LufthansaApiService lufthansaApiService;

	@GET
	@Path("airport/{id}")
	public AirportDto getAirport(final @PathParam("id") String airportCode) {
		final AirportDto airport = lufthansaApiService.getAirportInfo(airportCode);
		return airport;
	}

	@GET
	@Path("aircraft/{id}")
	public AirportDto getAircraft(final @PathParam("id") String aircraftCode) {
		final AirportDto ac = lufthansaApiService.getAircraftInfo(aircraftCode);

		return ac;
	}
}
