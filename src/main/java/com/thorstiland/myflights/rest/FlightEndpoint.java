package com.thorstiland.myflights.rest;

import java.util.Collections;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thorstiland.myflights.dto.ExtendedFlightDto;
import com.thorstiland.myflights.model.Flight;
import com.thorstiland.myflights.service.FlightService;

import io.swagger.annotations.Api;

@Api(tags = { "Flights" })
@Named
@Stateless
@Path("flight")
@RolesAllowed({ "user" })
public class FlightEndpoint {
	private static final Logger LOG = LoggerFactory.getLogger(FlightEndpoint.class);

	@Inject
	FlightService flightService;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public List<ExtendedFlightDto> getAll(@Context SecurityContext c) {
		if (c.getUserPrincipal()==null){
			return Collections.emptyList();
		}
		List<Flight> flights = flightService.findByUser(c.getUserPrincipal().getName());
		LOG.info("Loading all flights of user {} {}", c.getUserPrincipal(), flights);
		return ExtendedFlightDto.fromFlights(flights);
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public ExtendedFlightDto get(@PathParam("id") String id) {
		return ExtendedFlightDto.fromFlight(flightService.find(id));
	}

	@POST
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Flight create(Flight f, @Context SecurityContext s) {
		f.setUserId(s.getUserPrincipal().getName());
		return flightService.merge(f);
	}

	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public Flight put(@PathParam("id") String id, Flight f, @Context SecurityContext s) {
		f.setId(id);
		f.setUserId(s.getUserPrincipal().getName());
		return flightService.merge(f);
	}

	@DELETE
	@Path("/{id}")
	public void delete(@PathParam("id") String id) {
		flightService.delete(id);
	}

}
