package com.thorstiland.myflights.dto;

import java.time.LocalDateTime;

import com.thorstiland.myflights.model.Flight;

import lombok.Data;
@Data
public class FlightDto {
	protected String id;
	
	protected String userId;
	
	protected LocalDateTime departure;

	protected LocalDateTime arrival;
	
	protected String originAirportCode;
	protected String destinationAirportCode;
	protected String flightNo;
	protected String seat;
	protected String seatType;
	protected String bookingClass;
	protected String reason;
	protected String carrierCode;
	protected String aircraftCode;
	
	protected static void copyFromFlight(FlightDto dto, Flight entity){
		dto.setId(entity.getId());
		dto.setDeparture(entity.getDeparture().localDate());
		dto.setArrival(entity.getArrival().localDate());
		dto.setOriginAirportCode(entity.getFrom().getCode());
		dto.setDestinationAirportCode(entity.getTo().getCode());
		dto.setFlightNo(entity.getFlightNo());
		dto.setSeat(entity.getSeat());
		dto.setBookingClass(entity.getBookingClass());
		dto.setReason(entity.getReason());
		dto.setCarrierCode(entity.getCarrier().getAirlineCode());
		dto.setAircraftCode(entity.getAircraft().getAircraftCode());
	}
	
	public static FlightDto fromFlight(Flight entity){
		final FlightDto dto = new FlightDto();
		copyFromFlight(dto, entity);
		return dto;
	}
}
