package com.thorstiland.myflights.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.thorstiland.myflights.model.Flight;

import lombok.Data;

@Data
public class ExtendedFlightDto extends FlightDto{
	private AirportDto origin;
	private AirportDto destination;
	private AirlineDto carrier;
	private AircraftDto aircraft;
	
	public static ExtendedFlightDto fromFlight(Flight entity){
		ExtendedFlightDto dto = new ExtendedFlightDto();
		copyFromFlight(dto, entity);
		
		return dto;
	}
	public static List<ExtendedFlightDto> fromFlights(Collection<Flight> entities){
		List<ExtendedFlightDto> dtos = new ArrayList<>(entities.size());
		for (Flight entity : entities) {
			dtos.add(fromFlight(entity));
		}
		return dtos;
	}
	public static ExtendedFlightDto fromBasicDto(FlightDto basicFlight) {
		// TODO Auto-generated method stub
		return new ExtendedFlightDto();
	}
}
