package com.thorstiland.myflights.dto;

import lombok.Data;

@Data
public class AirportDto {
	private String code;
	private String name;
	private String openflightsId;

	private double latitude;
	private double longitude;
	private String timezoneId;
}
