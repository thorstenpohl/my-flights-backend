package com.thorstiland.myflights.dto;

import lombok.Data;

@Data
public class AirlineDto {
	private String airlineCode;
	private String airlineName;
}
