package com.thorstiland.myflights.dto;

import lombok.Data;

@Data
public class AircraftDto {
	private String aircraftType;
	private String tailsign;
	private String aircraftCode;
}
