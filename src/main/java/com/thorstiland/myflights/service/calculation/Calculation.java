package com.thorstiland.myflights.service.calculation;

import java.time.Duration;

import javax.ejb.Stateless;

import org.opensextant.geodesy.Angle;
import org.opensextant.geodesy.Geodetic2DArc;
import org.opensextant.geodesy.Geodetic2DPoint;
import org.opensextant.geodesy.Latitude;
import org.opensextant.geodesy.Longitude;

import com.thorstiland.myflights.model.Flight;

@Stateless
public class Calculation {
	public Duration calculateDuration(final Flight flight) {
		// As there are ZonedDateTimes the duration can be calculated easily.
		if (flight.getArrival() != null && flight.getDeparture() != null) {
			return Duration.between(flight.getDeparture().zonedDate(), flight.getDeparture().zonedDate());
		} else {
			return null;
		}

		// Duration durationWithoutTimezone =
		// Duration.between(flight.getDeparture(),
		// flight.getArrival());
		// double timezoneDifference = (flight.getAptFrom().getTimezone() +
		// 12.0)
		// - (flight.getAptTo().getTimezone() + 12.0);
		// Duration duration =
		// durationWithoutTimezone.plusHours(Math.round(timezoneDifference));
		//
		// return duration;

	}

	public double calculateDistance(final Flight flight) {

		Geodetic2DPoint departure = new Geodetic2DPoint(new Longitude(flight.getFrom().getLongitude(), Angle.DEGREES),
				new Latitude(flight.getFrom().getLatitude(), Angle.DEGREES));
		Geodetic2DPoint destination = new Geodetic2DPoint(new Longitude(flight.getTo().getLongitude(), Angle.DEGREES),
				new Latitude(flight.getTo().getLatitude(), Angle.DEGREES));

		Geodetic2DArc route = new Geodetic2DArc(departure, destination);

		double distance = route.getDistanceInMeters() / 1000.0 * 0.62137;
		return distance;
	}

	public static String formatDuration(Duration duration) {
		long seconds = duration.getSeconds();
		long absSeconds = Math.abs(seconds);
		String positive = String.format("%d:%02d", absSeconds / 3600, (absSeconds % 3600) / 60);
		return seconds < 0 ? "-" + positive : positive;
	}
}
