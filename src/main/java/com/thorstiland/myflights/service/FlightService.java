package com.thorstiland.myflights.service;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.Query;

import com.thorstiland.myflights.model.Flight;

@Stateless
@Named
public class FlightService extends CrudService<Flight> {

	public FlightService() {
		super(Flight.class);
	}
	@SuppressWarnings("unchecked")
	public List<Flight> findByUser(String userId) {
    	Query q =entityManager.createNativeQuery("db."+clazz.getSimpleName()+".find( {\"userId\": \""+userId+"\"} )",  clazz);
        
        
        return q.getResultList();
    }
}
